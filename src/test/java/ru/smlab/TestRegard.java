package ru.smlab;

import org.testng.annotations.Test;

public class TestRegard {

    @Test
    public void testSearch() {

        //1. Открыть главную страницу
        Base pageObject = new Base();
        pageObject.open();
        pageObject.checkThis(pageObject.logotipXpath);

        //2. Кликнуть по красной кнопке "Каталог" в левом верхнем углу
        pageObject.checkClick(pageObject.catalogXpath);

        //3. В выпавшей секции каталога выбрать: Комплектующие для ПК -> Материнские платы -> LGA 1200
        pageObject.checkClick(pageObject.pkXpath);
        pageObject.checkClick(pageObject.lga1200);

        //4. Найти 5-ю в списке материнскую плату, положить ее в корзину (белая прямоугольная кнопка напротив товара).
        pageObject.checkThis(pageObject.logotipPlataXpath);
        pageObject.checkClick(pageObject.plataXpath);

        //5. Дождаться пока пропадет всплывающее окно "Товар добавлен в корзину"
        pageObject.invisibilityOpject();

        //6. Кликнуть по красной кнопке "Каталог" в левом верхнем углу
        pageObject.checkClick(pageObject.catalogXpath);

        //7. В выпавшей секции каталога выбрать: Комплектующие для ПК -> Корпуса -> ATX
        pageObject.checkClick(pageObject.pkXpath);
        pageObject.checkClick(pageObject.atxXpath);

        //8. В фильтрах по товарам кликнуть чекбокс напротив производителя Aerocool
        pageObject.checkThis(pageObject.logotipKorpusXpath);
        pageObject.checkClick(pageObject.manufactura);

        //9. Найти 2-й в списке корпус, положить его в корзину
        pageObject.checkThis(pageObject.logotipKorpusAeroCoolXpath);
        pageObject.checkThis(pageObject.korpXpath);
        pageObject.checkClick(pageObject.korpXpath);


        //10. Во всплывающем окне "Товар добавлен в корзину" нажать на голубую кнопку "Перейти в корзину"
        pageObject.checkClick(pageObject.getCarzina);


        //11. Убедиться, что корзина не пустая, и что в ней содержатся ровно те наименования товаров, которые вы покупали.
        pageObject.checkThis(pageObject.carzina);
        pageObject.checkThis(pageObject.matXpath);
        pageObject.checkThis(pageObject.korpusXpath);
        pageObject.close();



    }
}
