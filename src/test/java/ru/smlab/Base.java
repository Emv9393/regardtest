package ru.smlab;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
public class Base {

    public  static WebDriver driver = null;

    static {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public String url = "https://www.regard.ru/";
    public String logotipXpath = "//div[@class='Grid_col__4bXWJ Grid_col-12__JMZP1 Header_content__wvWm4']/a[@class='Header_logo__gECVj']";
    public String logotipPlataXpath = "//div[@class='div page_title h1 ListingLayout_titleWrapper__cx52q']/h1[@class='PageTitle_title__7XQYG' and normalize-space(text())='Материнские платы LGA 1200']";
    public String logotipKorpusXpath = "//h1[@class='PageTitle_title__7XQYG' and text()='Корпуса ATX']";
    public String logotipKorpusAeroCoolXpath = "//div[@class='FilterTags_group__1LM5m']/span[text()='Производитель:']";
    public String korpXpath = "//div[@class='ListingRenderer_row__0VJXB']/div[2]//button[@type='button']";
    public String plataXpath = "//div[@class='ListingRenderer_row__0VJXB']/div[5]//button[@type='button']";
    public String manufactura = "//label[@for='id-AeroCool-AeroCool' and contains(text(),'AeroCool')]";
    public String matXpath = "//a[@class='BasketItem_link__qYvgV']/span[contains(text(),'Материнская плата ')]";
    public String korpusXpath = "//a[@class='BasketItem_link__qYvgV']/span[contains(text(),'Корпус')]";
    public String catalogXpath = "//span[text()='Каталог']";
    public String carzina = "//h1[@class='PageTitle_title__7XQYG' and text()='Корзина']";
    public String getCarzina = "//a[@class='Button_button__GeQ2O Button_small___Zwz0 Button_primaryBlue__Y7daK BasketAdded_actionBtn__euLl_' and text()='Перейти в корзину']";
    public String atxXpath = "//a[@class='Category_link__kk88E link_gray' and text()='ATX']";
    public String lga1200 = "//a[@class='Category_link__kk88E link_gray' and text()='LGA 1200']";
    public String pkXpath = "//div[@class='Catalog_mainCategoryName__xzGxz' and normalize-space(text())='Комплектующие для ПК']";

    public void open() {

        driver.get(url);
    }

    public void checkClick(String xpathFoClick){
        WebElement checClick = new WebDriverWait(driver, Duration.ofSeconds(25),Duration.ofSeconds(1))
                .until(ExpectedConditions.elementToBeClickable(By.xpath(xpathFoClick)));
        checClick.click();
    }

    public void invisibilityOpject(){
        Boolean invisibilityOpject = new WebDriverWait(driver, Duration.ofSeconds(25),Duration.ofSeconds(1))
                .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//h3[@class='h3 BasketAdded_title__5YQko' and text()='Товар добавлен в корзину']")));
    }


    public boolean checkThis(String xpath) {
        WebElement check = new WebDriverWait(driver, Duration.ofSeconds(15),Duration.ofSeconds(1))
                .until(driver -> driver.findElement(By.xpath(xpath)));

        return check.isDisplayed();
    }


    public void close() {

        driver.quit();
    }

}
